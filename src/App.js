import React from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './component/Home';
import Menu from './component/Menu';
import {BrowserRouter as Router, Switch,Route} from 'react-router-dom'
import View from './component/View';
import Add from './component/Add';
import Edit from './component/Edit';
function App() {
  return (
    <div>
      <Router>
        <Menu />
        <Switch>
          <Route path="/" exact component ={Home}/>
          <Route path="/View/:id" component ={View}/>
          <Route path="/add" component={Add}/>
          <Route path="/edit/:id" component={Edit}/>
        </Switch>
      </Router>
    </div>
   
  );
}

export default App;
