import React, { Component } from 'react';
import axios from 'axios';
import {Row,Form ,Button} from 'react-bootstrap'
export default class Edit extends Component {
    constructor(props){
        super(props);
        this.state={
            TITLE:"",
            DESCRIPTION:"",    
        }
    }
    onChangeUpdate(event){
        event.preventDefault();
        let Update ={
            TITLE:this.state.TITLE,
            DESCRIPTION:this.state.DESCRIPTION,   
        }
        var ID = this.props.match.params.id;
        axios.put(`http://110.74.194.124:15011/v1/api/articles/${ID}`,Update).then(res =>{
            console.log(res.data);
            alert(res.data.MESSAGE);
        })
    }
    componentWillMount(){
        const ID = this.props.match.params.id;
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${ID}`).then(res=>{  
            this.setState({
                data: res.data.DATA,
                TITLE: res.data.DATA.TITLE,
                DESCRIPTION: res.data.DATA.DESCRIPTION,    
            }) ;        
        })
    }
    changeHandler= (event) => {
        this.setState({ [event.target.name]: event.target.value}
            )
    }   
    render() {
        return (
            <div className ="container">  
            <Form onSubmit ={(e)=>this.onChangeUpdate(e)}>
                <h4>Add New Articles</h4>
                <Row>
                <div className="col-md-8 col-sm-12">
                    <Form.Group controlId="formGroupTitle">
                        <Form.Label>TITLE</Form.Label>
                        <Form.Control type="text" placeholder="Enter title" name="TITLE" value={this.state.TITLE} onChange={(e)=>this.changeHandler(e)}/>
                    </Form.Group>
                    <Form.Group controlId="formGroupDescription">
                        <Form.Label>DESCRIPTION</Form.Label>
                        <Form.Control type="text" placeholder="Enter description" name="DESCRIPTION" value={this.state.DESCRIPTION} onChange={(e)=>this.changeHandler(e)}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" onChange={this.onChangeUpdate}>
                         Update
                    </Button>
                    </div>
                 
                    </Row>
            </Form>
    </div>
        )
    }
}