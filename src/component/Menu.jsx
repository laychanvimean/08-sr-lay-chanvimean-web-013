import React from 'react'
import {Link} from 'react-router-dom'
import { Navbar,Nav,FormControl,Form,Button} from 'react-bootstrap'
function Menu() {
    return (
        <div>
                   <Navbar bg="light" variant="light">
                    <Navbar.Brand href="#home">AMS</Navbar.Brand>
                    <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    </Nav>
                    <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-primary">Search</Button>
                    </Form>
                </Navbar>
        </div>
      
    )
}

export default Menu
